#include <string>
#include <stack>
using namespace std;
class Solution
{
public:
    bool isValid(string s)
    {
        stack<string> s1;
        // bool falg= true;
        for (int i = 0; i < s.size(); i++)
        {
            if (s.at(i) == '(' || s.at(i) == '{' || s.at(i) == '[')
            {
                if (s.at(i) == '(')
                {
                    s1.push("(");
                }
                if (s.at(i) == '{')
                {
                    s1.push("{");
                }
                if (s.at(i) == '[')
                {
                    s1.push("[");
                }
            }
            if(s.at(i)==')'&&s1.top()=="("){
                s1.pop();
            }
            else if(s.at(i)==')'&&(s1.size()==0||s1.top()!="("))
            {
                return false;
            }
            if(s.at(i)=='}'&&s1.top()=="{"){
                s1.pop();
            }
            else if(s.at(i)=='}'&&(s1.size()==0||s1.top()!="{")){
                return false;
            }
             if(s.at(i)==']'&&s1.top()=="["){
                s1.pop();
            }
            else if(s.at(i)==']'&&(s1.size()==0||s1.top()!="[")){
                return false;
            }
        }
        if (s1.size() == 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
};