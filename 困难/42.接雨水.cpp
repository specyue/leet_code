#include <vector>
using namespace std;
//当前可以容纳的高度==它两边高度的较小值减去当前高度的值
//如果两边高度的较小值==0,则当前不能容水
//如果当前是边界,不能容纳水
class Solution
{
public:
    int trap(vector<int> &height)
    {
        // [2,0,2]
        int len = height.size();
        int sum = 0;
        for (int i = 0; i < len; i++)
        {
            int left = i;
            int max_left = height[left];
            int right = i;
            int max_right = height[right];
            while (left > 0)
            {
                max_left = max(max_left,height[--left]);
            }
            while(right<len-1){
                max_right = max(max_right,height[++right]);
            }
            int res = min(max_left,max_right);
            sum+=res-height[i];
        }
        return sum;
    }
};