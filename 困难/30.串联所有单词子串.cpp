#include <string>
#include <vector>
#include <iostream>
#include <algorithm>
#include <map>
using namespace std;
class Solution
{

    //匹配字符串太长的时候时间性能过不去

public:
    map<string, vector<int>> hash;
    vector<string> strs;
    vector<int> res;
    void strStr(string haystack, string needle)
    {
        if (needle == "")
        {
            res.push_back(0);
        }
        int len = haystack.length();
        int n_len = needle.length();
        bool flag = true;
        int i = 0;
        while (i != len)
        {
            if (haystack[i] == needle[0])
            {
                for (int j = 0; j < n_len; j++)
                {
                    if (needle[j] != haystack[i + j])
                    {
                        // flag = false;
                        break;
                    }
                    if (j == n_len - 1 && needle[j] == haystack[i + j])
                    {
                        res.push_back(i);
                    }
                }
            }
            i++;
        }
        // return -1;
    }
    void swap(string *a, string *b)
    {
        string m;
        m = *a;
        *a = *b;
        *b = m;
    }
    void perm(vector<string> &words, int k, int m)
    {
        string tmp;
        int i;
        if (k == m)
        {
            for (i = 0; i < m; i++)
            {
                tmp += words.at(i);
            }
            strs.push_back(tmp);
        }
        else
        {
            for (i = k; i < m; i++)
            {
                swap(&words[k], &words[i]);
                perm(words, k + 1, m);
                swap(&words[k], &words[i]);
            }
        }
    }

    int removeDuplicates(vector<int> &nums)
    {
        int i = 1;
        for (i; i < nums.size(); i++)
        {
            if (nums.at(i) == nums.at(i - 1))
            {
                nums.erase(i + nums.begin());
                i = 1;
            }
        }
        i = 1;
        for (i; i < nums.size(); i++)
        {
            if (nums.at(i) == nums.at(i - 1))
            {
                nums.erase(i + nums.begin());
                i = 1;
            }
        }
        return nums.size();
    }

public:
    vector<int> findSubstring(string s, vector<string> &words)
    {
        //拼接出所有可能的子串,然后寻找子串在不在母串里
        if (s == "" || words.size() == 0)
        {
            return {};
        }
        perm(words, 0, words.size());
        for (int i = 0; i < strs.size(); i++)
        {
            // cout << strs[i] << endl;
            strStr(s, strs[i]);
        }
        sort(res.begin(), res.end());
        removeDuplicates(res);
        return res;
    }
};

int main(int argc, char **argv)
{
    string s = "aabbcceeaabb";
    vector<string> words;
    vector<int> result;
    words.push_back("aa");
    words.push_back("bb");
    Solution s1;
    result = s1.findSubstring(s, words);
    cout << s1.strs.size() << endl;
    for (int j = 0; j < result.size(); j++)
    {
        cout << result[j] << endl;
    }
}