#include <vector>
#include <string>
using namespace std;

struct ListNode
{
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(NULL) {}
};

//分治算法
class Solution
{
public:
    ListNode *merge(ListNode *l1, ListNode *l2)
    {
        ListNode *proot, *cur;
        if (l1 == NULL && l2 == NULL)
        {
            return NULL;
        }
        if (l1 != NULL && l2 != NULL)
        {
            if (l1->val <= l2->val)
            {
                proot = l1;
                l1 = l1->next;
            }
            else
            {
                proot = l2;
                l2 = l2->next;
            };
            cur = proot;
            while (l1 && l2)
            {
                if (l1->val <= l2->val)
                {
                    cur->next = l1;
                    l1 = l1->next;
                }
                else
                {
                    cur->next = l2;
                    l2 = l2->next;
                }
                cur = cur->next;
            }
            if (l1)
            {
                cur->next = l1;
            }
            if (l2)
            {
                cur->next = l2;
            }
            return proot;
        }
        if(l1!=NULL&&l2==NULL){
            return l1;
        }
        if(l1==NULL&&l2!=NULL){
            return l2;
        }
        return NULL;
    }

    ListNode *solve(vector<ListNode *> &lists, int start, int end)
    {
        if (start == end)
        {
            return lists[start];
        }
        else
        {
            int mid = (start + end) >> 1;
            ListNode *lnode = solve(lists, start, mid);
            ListNode *rnode = solve(lists, mid + 1, end);

            return merge(lnode, rnode);
        }
    }
    ListNode *mergeKLists(vector<ListNode *> &lists)
    {
        int len = lists.size();
        if (len == 0)
        {
            return NULL;
        }
        else
        {
          return   solve(lists, 0, len - 1);
        }
    }
};