#include <vector>
#include <map>
#include <algorithm>
using namespace std;
//时间复杂度O(n),空间复杂度为常数级别

//思想
//先遍历一遍数组,建立一个hash表
//然后再遍历一遍数组,记录当前找到的最小值,例如nums.at(i)最小
//那么如果hash[nums.at(i)+1]不是1的话就代表不连续,就把nums.at(i)+1暂时存到result

class Solution
{
public:
    int firstMissingPositive(vector<int> &nums)
    {
        if (nums.size() == 0)
        {
            return 0;
        }
        sort(nums.begin(), nums.end());
        int i;
        for (i = 0; i < nums.size() - 1; i++)
        {
            if (nums[i] >= 0 && nums[i + 1] - nums[i] > 1)
            {
                return nums[i] + 1;
            }
        }
        return nums[nums.size() - 1] + 1;
    }
};