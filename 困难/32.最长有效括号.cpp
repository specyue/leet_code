#include <string>
#include <stack>
using namespace std;
class Solution
{
public:
    int N = 0;
    int longestValidParentheses(string s)
    {
        stack<string> kuohao;
        int len = s.size();
        for (int i = 0; i < len; i++)
        {
            if (s.at(i) == '(')
            {
                kuohao.push("(");
            }
            if (s.at(i) == ')' && kuohao.size() != 0)
            {
                N+=2;
                kuohao.pop();
            }
        }
        return N;
    }
};