#include <vector>
#include <string>
using namespace std;
class Solution
{
public:
    vector<vector<int>> threeSum(vector<int> &nums)
    {
        vector<vector<int>> a;
        vector<int> tmp;
        int len = nums.size();
        for (int i = 0; i < len; i++)
        {
            for (int j = 0; j < len; j++)
            {
                for (int h = 0; h < len; h++)
                {
                    if (nums.at(i) + nums.at(j) + nums.at(h) == 0 && nums.at(i) != nums.at(j) && nums.at(i) != nums.at(h) && nums.at(j) != nums.at(h))
                    {
                        tmp.push_back(nums.at(i));
                        tmp.push_back(nums.at(j));
                        tmp.push_back(nums.at(h));
                        a.push_back(tmp);
                        tmp.erase(tmp.begin(), tmp.end());
                    }
                }
            }
        }
        return a;
    }
};