
// 1.设置:指针l,指针r,指针r指向指针l的后n个
// 2.当指针r指向尾节点的时候,指针l就是结果

//  Definition for singly-linked list.
#include <iostream>
using namespace std;
struct ListNode
{
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(NULL) {}
};

class Solution
{
public:
    ListNode *removeNthFromEnd(ListNode *head, int n)
    {
        ListNode *L, *R, *LL;
        // LL = head;
        L = head;
        R = head->next;
        if (R != NULL)
        {
            for (int i = 0; i <= n - 1; i++)
            {
                R = R->next;
            }
            while (R != NULL)
            {
                R = R->next;
                L = L->next;
            }
            L->next = L->next->next;
            return head;
        }
        else
        {
            return NULL;
        }
    }
};