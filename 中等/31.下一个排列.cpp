#include <vector>
using namespace std;
class Solution
{
public:
    void swap(int *a, int *b)
    {
        int tmp;
        tmp = *a;
        *a = *b;
        *b = tmp;
    }
    void nextPermutation(vector<int> &nums)
    {
        int i = 1;
        int a;
        int b;
        for (i; i < nums.size() - 1; i++)
        {
            if (nums.at(i - 1) < nums.at(i) && nums.at(i) > nums.at(i + 1))
            {
                
                swap(&nums.at(i), &nums.at(i - 1));
                return ;
            }
        }
        i = i-1;
        if (i == nums.size() - 2 && nums.at(i)<nums.at(i+1))
        {
            // swap(&nums.at(i), &nums.at(i + 1));
            nums.reserve(nums.size());
            return ;
        }
         if (i == nums.size() - 2 && nums.at(i)>nums.at(i+1))
        {
            swap(&nums.at(i), &nums.at(i + 1));
            // nums.reserve(nums.size());
            return ;
        }
    }
};