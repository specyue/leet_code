#include <vector>
#include <algorithm>
using namespace std;

class Solution
{
public:
    bool canJumpH(int position, vector<int> &nums)
    {
        if (position == nums.size() - 1)
        {
            return true;
        }
        int featureJump;
        if (position + nums[position] >= nums.size() - 1)
        {
            featureJump = nums.size() - 1;
        }
        else
        {
            featureJump = position + nums[position];
        }
        for(int i = position+1;i<featureJump;i++){
            if(canJumpH(i,nums)){
                return true;
            }
        }
        return false;
    }
    bool canJump(vector<int> &nums)
    {
        return canJumpH(0, nums);
    }
};