#include <string>
#include <iostream>
#include <cstring>
#include <map>
using namespace std;
//暴力法
class Solution1
{
public:
    int lengthOfLongestSubstring(string s)
    {
        int max_num = 0;
        for (int i = 0; i < s.length(); i++)
        {
            for (int j = i + 1; j < s.length() + 1; j++)
            {
                if (quote(s, i, j))
                {
                    max_num = max(max_num, j - i);
                }
            }
        }
        return max_num;
    }

    bool quote(string s, int i, int j)
    {
        map<char, int> map1;
        for (int start = i; start < j; i++)
        {
            //如果这个字符的hash已经存在了就代表是重复的字符串,返回false
            if (map1.find(s[start]) != map1.end())
            {
                return false;
            }
            map1[s[start]] = start;
        }
        return true;
    }
};

//滑动窗口
//end如果没有hash,则hash一下,如果已经hash就说明前面有重复的元素
//有重复的元素就用start去删除hash的值
class Solution2
{
public:
    int lengthOfLongestSubstring(string s)
    {
        int start = 0;
        int end = 0;
        int max_num = 0;
        map<char, int> hash;
        int len = s.size();
        while (start < len && end < len)
        {
            if (hash.find(s[end]) == hash.end())
            {
                //没有重复
                hash[s[end++]] = end;
                max_num = max(max_num, end - start);
            }
            else
            {
                hash.erase(s[start++]);
            }
        }
        return max_num;
    }
};

class Solution
{
public:
    int lengthOfLongestSubstring(string s)
    {
        int start = 0;
        int end = 0;
        int len = s.length();
        int max_num = 0;
        map<char, int> hash;
        for(start,end;end<len;end++)
        {
            if (hash.find(s[end]) != hash.end())
            {
                //如果找到重复元素,则start直接变成重复元素的下一个元素
                start = max(hash.find(s[end])->second+1,start);
                
            }
            max_num=max(max_num,end-start+1);
            hash[s[end]]=end;
        }
        return max_num;
    }
};