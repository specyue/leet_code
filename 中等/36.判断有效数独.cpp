#include <vector>
#include <map>
using namespace std;

class Solution
{
public:
    bool isValidSudoku(vector<vector<char>> &board)
    {
        int row = board.size();
        int column = board[0].size();
        // map<char ,int >tmp;
        map<int, map<char, int>> hash_row;
        map<int, map<char, int>> hash_column;
        map<int, map<char, int>> hash_box;
        // int box;
        for (int i = 0; i < row; i++)
        {
            for (int j = 0; j < column; j++)
            {
                if (board[i][j] != '.')
                {
                    // tmp[board[i][j]] = 1;
                    if (!hash_row[i][board[i][j]] )
                    {
                        hash_row[i][board[i][j]] = 1;
                    }
                    if (hash_row[i][board[i][j]] == 1)
                    {
                        return false;
                    }
                    if (!hash_column[j][board[i][j]] )
                    {
                        hash_column[j][board[i][j]] = 1;
                    }
                    if (hash_column[j][board[i][j]] == 1)
                    {
                        return false;
                    }
                    // box = (i / 3) * 3 + j / 3;
                    if (!hash_box[(i / 3) * 3 + j / 3][board[i][j]] )
                    {
                        hash_box[(i / 3) * 3 + j / 3][board[i][j]] = 1;
                    }
                    if (hash_box[(i / 3) * 3 + j / 3][board[i][j]] == 1)
                    {
                        return false;
                    }
                }
            }
        }
        return true;
    }
};

// [ [ "5", "3", ".", ".", "7", ".", ".", ".", "." ],
//  [ "6", ".", ".", "1", "9", "5", ".", ".", "." ], 
//  [ ".", "9", "8", ".", ".", ".", ".", "6", "." ],
//   [ "8", ".", ".", ".", "6", ".", ".", ".", "3" ]
//   , [ "4", ".", ".", "8", ".", "3", ".", ".", "1" ],
//    [ "7", ".", ".", ".", "2", ".", ".", ".", "6" ], 
//    [ ".", "6", ".", ".", ".", ".", "2", "8", "." ]
//    , [ ".", ".", ".", "4", "1", "9", ".", ".", "5" ],
//     [ ".", ".", ".", ".", "8", ".", ".", "7", "9" ] ]