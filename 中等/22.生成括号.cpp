#include <iostream>
#include <vector>
#include <string>
using namespace std;

class Solution
{
public:
    vector<string> res;
    int N;
    void dfs(int l, int r, string has)
    {
        if (l > N)
        {
            return;
        }
        if (r > l)
        {
            return;
        }
        if(l==r&&r==N){
            res.push_back(has);
            return ;
        }
        dfs(l+1,r,has+"(");
        dfs(l,r+1,has+")");
    }
    vector<string> generateParenthesis(int n)
    {
        // string res;
        N = n;
        if (n == 0)
        {
            return {};
        }
        else
        {
            dfs(0, 0, "");
            return res;
        }
    }
};