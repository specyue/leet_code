#include <vector>
#include <set>
#include <algorithm>
using namespace std;

// 给定一个数组 candidates 和一个目标数 target ，找出 candidates 中所有可以使数字和为 target 的组合。
// candidates 中的每个数字在每个组合中只能使用一次。
//和组合总数1的区别是每个数字只能出现使用一次,其实也好办,就是在回溯的时候把candidates对应的数字去掉

class Solution1
{
public:
    vector<vector<int>> result;
    void subSum(vector<int> &cadidates, vector<int> tmp_res, int target, int sub)
    {
        vector<int> tmp_cadidates = cadidates;
        if (target - sub == 0)
        {
            tmp_res.push_back(sub);
            result.push_back(tmp_res);
            return;
        }
        if (target - sub > 0)
        {
            tmp_res.push_back(sub);
            target = target - sub;
            for (int i = 0; i < cadidates.size(); i++)
            {
                int num = cadidates.at(i);
                tmp_cadidates = cadidates;
                tmp_cadidates.erase(tmp_cadidates.begin() + i);
                subSum(tmp_cadidates, tmp_res, target, num);
            }
        }
        if (target - sub < 0)
        {
            return;
        }
    }
    vector<vector<int>> combinationSum2(vector<int> &candidates, int target)
    {
        vector<int> tmp_res;
        vector<int> tmp_cadidates = candidates;
        for (int i = 0; i < candidates.size(); i++)
        {
            int num = candidates.at(i);
            tmp_cadidates = candidates;
            tmp_cadidates.erase(tmp_cadidates.begin() + i);
            subSum(tmp_cadidates, tmp_res, target, num);
        }
        for (int j = 0; j < result.size(); j++)
        {
            //先把每个数组排序
            sort(result[j].begin(), result[j].end());
        }
        set<vector<int>> set_tmp(result.begin(), result.end());
        result.assign(set_tmp.begin(), set_tmp.end());
        return result;
    }
};

class Solution
{
public:
    vector<vector<int>> ans;
    void dpSum(vector<int> &candidates, int target, vector<int> vec, int begin)
    {
        if (target < 0)
            return;
        else if (target == 0)
        {
            ans.push_back(vec);
        }
        else
        {
            for (int i = begin; i < candidates.size() && candidates[i] <= target; ++i)
            {
                //目的是去重
                if (i > begin && candidates[i - 1] == candidates[i])
                    continue;
                vec.push_back(candidates[i]);
                dpSum(candidates, target - candidates[i], vec, i + 1);
                vec.pop_back();
            }
        }
    }

    vector<vector<int>> combinationSum2(vector<int> &candidates, int target)
    {
        vector<int> vec;
        sort(candidates.begin(), candidates.end()); //排序
        dpSum(candidates, target, vec, 0);
        return ans;
    }
};
