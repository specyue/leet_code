// 首先，该函数会根据需要丢弃无用的开头空格字符，直到寻找到第一个非空格的字符为止。

// 当我们寻找到的第一个非空字符为正或者负号时，则将该符号与之后面尽可能多的连续数字组合起来，作为该整数的正负号；假如第一个非空字符是数字，则直接将其与之后连续的数字字符组合起来，形成整数。

// 该字符串除了有效的整数部分之后也可能会存在多余的字符，这些字符可以被忽略，它们对于函数不应该造成影响。

// 注意：假如该字符串中的第一个非空格字符不是一个有效整数字符、字符串为空或字符串仅包含空白字符时，则你的函数不需要进行转换。

// 在任何情况下，若函数不能进行有效的转换时，请返回 0。

#include <iostream>
#include <string>
using namespace std;
class Solution
{
public:
    int convert2shuzi(string i)
    {
        if (i[0] == '0')
        {
            return 0;
        }
        if (i[0] == '1')
        {
            return 1;
        }
        if (i[0] == '2')
        {
            return 2;
        }
        if (i[0] == '3')
        {
            return 3;
        }
        if (i[0] == '4')
        {
            return 4;
        }
        if (i[0] == '5')
        {
            return 5;
        }
        if (i[0] == '6')
        {
            return 6;
        }
        if (i[0] == '7')
        {
            return 7;
        }
        if (i[0] == '8')
        {
            return 8;
        }
        if (i[0] == '8')
        {
            return 8;
        }
    }

    int myAtoi(string str)
    {
        int ZF_flag = 0;
        int len = str.length();
        int sum = 0;
        char ZF;
        for (int i = 0; i < len; i++)
        {
            if (str[i] == ' ')
            {
                i++;
                continue;
            }
            if (ZF_flag == 0 && (str[i] == '+' || str[i] == '-'))
            {
                //第一次遇到正负号
                ZF = str[i];
                ZF_flag += 1;
                i++;
                continue;
            }
            if (ZF_flag = 1 && (str[i] == '+' || str[i] == '-'))
            {
                //第er次遇到正负号
                return 0;
            }
            if (str[i] > '0' && str[i] < '9')
            {
                int shuzi = convert2shuzi(&str[i]);
                sum = sum * 10 + shuzi;
            }
            if (str[i] < '0' || str[i] > '9')
            {
                if (ZF == '+')
                {
                    return sum;
                }
                else if (ZF == '-')
                {
                    sum = sum * -1;
                    return sum;
                }
            }
        }
    }
};

int main(int argc, char **argv)
{
    Solution s;
    s.myAtoi("225");
}