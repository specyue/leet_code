#include <vector>
#include <iostream>
#include <set>
using namespace std;
class Solution
{
public:
    vector<vector<int>> res;
    void prem(vector<int> nums, int i, int j)
    {
        if (i == j)
        {
            res.push_back(nums);
        }
        else{
            for(int k = i;k<j+1;k++){
                swap(nums[i],nums[k]);
                prem(nums,i+1,j);
                swap(nums[i],nums[k]);
            }
        }
    }
    vector<vector<int>> permute(vector<int> &nums)
    {
        prem(nums, 0, nums.size() - 1);
        return res;
    }
};

