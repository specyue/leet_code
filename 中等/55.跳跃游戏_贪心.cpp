#include <vector>
using namespace std;

class Solution
{
public:
    bool canJump( vector<int> &nums)
    {
        if (nums.size() == 0)
        {
            return false;
        }
        int last_position = nums.size() - 1;
        for (int i = last_position; i >= 0; i--)
        {
            if (nums[i] + i >= last_position)
            {
                last_position = i;
            }
        }
        return last_position==0;
    }
};