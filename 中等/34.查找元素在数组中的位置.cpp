#include <vector>
using namespace std;
class Solution
{
public:
    vector<int> searchRange(vector<int> &nums, int target)
    {
        vector<int> res;
        for (int i = 0; i < nums.size(); i++)
        {
            if (nums.at(i) == target)
            {
                res.push_back(i);
                break;
            }
        }
        for (int j = nums.size() - 1; j >= 0; j--)
        {
            if (nums.at(j) == target)
            {
                res.push_back(j);
                break;
            }
        }
        if (res.size() == 0)
        {
            res.push_back(-1);
            res.push_back(-1);
        }
        return res;
    }
};