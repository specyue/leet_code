#include <vector>
#include <string>
#include <algorithm>
#include <map>
using namespace std;
class Solution
{
public:
    vector<vector<string>> res;
    //思路:将数组里所有的字符串排序,存到新数组new_strs里,对应下标不变
    //遍历new_strs,拿当前的和往后所有的比较,如果相等,扔到一个数组里,遍历到底将当前的扔到数组里
    //删去对于下标,再从头遍历,直到new_strs,size()==0
    vector<vector<string>> groupAnagrams(vector<string> &strs)
    {
        vector<string> strss = strs;
        vector<vector<char>> new_strs;
        map<int, bool> hash;

        for (int i = 0; i < strs.size(); i++)
        {
            hash[i] = true;
        }
        for (int i = 0; i < strss.size(); i++)
        {
            vector<char> tmp;
            for (int j = 0; j < strss[i].size(); j++)
            {
                tmp.push_back(strss[i][j]);
            }
            sort(tmp.begin(), tmp.end());
            new_strs.push_back(tmp);
        }
        for (int i = 0; i < new_strs.size(); i++)
        {
            vector<string> tmpp;
            for (int j = 1; j < new_strs.size(); j++)
            {
                if (new_strs[i] == new_strs[j] && hash[j] )
                {

                    tmpp.push_back(strs[j]);
                    hash[j] = false;
                }
            }
            if(hash[i]){
                 tmpp.push_back(strs[i]);
            }
           
            res.push_back(tmpp);
            hash[i] == false;
            // l++;
        }
        for(int k = 0;k<res.size();k++){
            if(res[k].size()==0){
                res.erase(res.begin()+k);
            }
        }
        return res;
    }
};