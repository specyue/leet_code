// # 题目
// 给定一个包含 n 个整数的数组 nums 和一个目标值 target，判断 nums 中是否存在四个元素 a，b，c 和 d ，使得 a + b + c + d 的值与 target 相等？找出所有满足条件且不重复的四元组。

// # 思路
// 1.先把数组排序
// 2.固定住两个基础指针,注意去重
// 3.一个左指针在右边基础指针的右边,一个右指针,在数组尾部
// 4.如果sum>target则左移右指正,反之则右移左指针

#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;
class Solution
{
public:
    vector<vector<int>> fourSum(vector<int> &nums, int target)
    {
        int len = nums.size();
        vector<vector<int>> result;
        vector<int> tmp;
        sort(nums.begin(), nums.end());
        for (int i = 0; i < len - 3; i++)
        {
            //去重
            if (i > 0 && nums[i] == nums[i - 1])
            {
                continue;
            }
            if (nums[i] + nums[i + 1] + nums[i + 2] + nums[i + 3] > target)
            {
                break;
            }
            if (nums[i] + nums[len - 1] + nums[len - 2] + nums[len - 3] < target)
            {
                continue;
            }
            for (int j = i + 1; j < len - 2; j++)
            {
                if (j > i && nums[j] == nums[j - 1])
                {
                    continue;
                }
                if (nums[i] + nums[j] + nums[j + 1] + nums[j + 2] > target)
                {
                    break;
                }
                if (nums[i] + nums[j] + nums[len - 1] + nums[len - 2] < target)
                {
                    continue;
                }
                int start = j + 1;
                int end = len - 1;
                while (start < end)
                {
                    int sum = nums[i] + nums[j] + nums[start] + nums[end];
                    if (sum == target)
                    {
                        tmp.push_back(nums[i]);
                        tmp.push_back(nums[j]);
                        tmp.push_back(nums[start]);
                        tmp.push_back(nums[end]);
                        result.push_back(tmp);
                        //去重
                        while (start < end && nums[start] == nums[start + 1])
                        {
                            start += 1;
                        }
                        while (start < end && nums[end] == nums[end - 1])
                        {
                            end -= 1;
                        }
                    }
                    else if (sum < target)
                    {
                        while (start < end && nums[start] == nums[++start])
                            ;
                    }
                    else if (sum > target)
                    {
                        while (start < end && nums[end] == nums[--end])
                            ;
                    }
                }
            }
        }
        return result;
    }
};

int main(int argc, char **argv)
{
    return 0;
}