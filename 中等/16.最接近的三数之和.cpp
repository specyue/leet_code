#include <vector>
#include <cmath>
#include <map>
using namespace std;
class Solution
{
public:
    int threeSumClosest(vector<int> &nums, int target)
    {
        int len = nums.size();
        map<int, int> hash;
        int min_num = __INT32_MAX__;
        for (int i = 0; i < len; i++)
        {
            int L = i - 1;
            int R = i + 1;
            while (L >= 0 && R <= len - 1)
            {
                int tmp_l = L;
                int tmp_r = R;
                if (L < 0)
                {
                    tmp_l = 0;
                }
                if (R > len - 1)
                {
                    tmp_r = len - 1;
                }
                int t;
                if(target<0&&nums.at(tmp_l) + nums.at(tmp_r) + nums.at(i)<=0){
                     t = abs(target-nums.at(tmp_l) + nums.at(tmp_r) + nums.at(i));
                }
                if(target<0&&nums.at(tmp_l) + nums.at(tmp_r) + nums.at(i)>0){
                    t = nums.at(tmp_l) + nums.at(tmp_r) + nums.at(i)-target;
                }
                if(target>=0&&nums.at(tmp_l) + nums.at(tmp_r) + nums.at(i)>0)
                {
                    t = abs(target - nums.at(tmp_l) - nums.at(tmp_r) - nums.at(i));
                }
                if(target>=0&&nums.at(tmp_l) + nums.at(tmp_r) + nums.at(i)<=0){
                   t = abs(target - nums.at(tmp_l) - nums.at(tmp_r) - nums.at(i));
                }
                hash[t] = nums.at(tmp_l) + nums.at(tmp_r) + nums.at(i);
                min_num = min(min_num, t);
                L--;
                R++;
            }
        }
        return hash[min_num];
    }
};