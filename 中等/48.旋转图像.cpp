#include <vector>
using namespace std;
class Solution
{
public:
    void rotate(vector<vector<int>> &matrix)
    {
        int chang = matrix.size();
        int kuan = chang;
        for (int i = 0; i < chang ; i++)
        {
            for (int j = 0; j < kuan / 2; j++)
            {
                int tmp = matrix[i][j];
                matrix[i][j] = matrix[j][i];
                matrix[j][i] = tmp;
            }
        }
       for(int i = 0;i<chang;i++){
           for(int j = 0;j<kuan/2;j++){
               int tmp = matrix[i][j];
               matrix[i][j] = matrix[i][kuan-1-i];
               matrix[i][kuan-1-i] = tmp;
           }
       }
    }
};