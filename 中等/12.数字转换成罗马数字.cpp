// 字符          数值
// I             1
// V             5
// X             10
// L             50
// C             100
// D             500
// M             1000
// 例如， 罗马数字 2 写做 II ，即为两个并列的 1。12 写做 XII ，即为 X + II 。 27 写做  XXVII, 即为 XX + V + II 。

// 通常情况下，罗马数字中小的数字在大的数字的右边。但也存在特例，例如 4 不写做 IIII，而是 IV。数字 1 在数字 5 的左边，所表示的数等于大数 5 减小数 1 得到的数值 4 。同样地，数字 9 表示为 IX。这个特殊的规则只适用于以下六种情况：

// I 可以放在 V (5) 和 X (10) 的左边，来表示 4 和 9。
// X 可以放在 L (50) 和 C (100) 的左边，来表示 40 和 90。 
// C 可以放在 D (500) 和 M (1000) 的左边，来表示 400 和 900。
// 给定一个整数，将其转为罗马数字。输入确保在 1 到 3999 的范围内。
#include <string>
#include <map>
#include <iostream>
using namespace std;
class Solution
{
public:
    string str = "";

public:
    string num2Rom(int num)
    {
        map<int, string> hash;
        hash[1] = "I";
         hash[4] = "IV";
        hash[5] = "V";
        hash[10] = "X";
        hash[50] = "L";
        hash[100] = "C";
        hash[500] = "D";
        hash[1000] = "M";
        hash[900] = "CM";
        hash[400] = "CD";
        hash[90] = "XC";
        hash[40] = "XL";
        hash[9] ="IX";
        while (num != 0)
        {
            if (num >= 1000)
            {
                str += hash[1000];
                num -= 1000;
                continue;
            }
            if (num >= 900 && num < 1000)
            {
                str += hash[900];
                num -= 900;
                continue;
            }
            if (num >= 500 && num < 900)
            {
                str += hash[500];
                num -= 500;
                continue;
            }
            if (num >= 400 && num < 500)
            {
                str += hash[400];
                num -= 400;
                continue;
            }
            if (num >= 100 & num < 400)
            {
                str += hash[100];
                num -= 100;
                continue;
            }
            if (num >= 90 & num < 100)
            {
                str += hash[90];
                num -= 90;
                continue;
            }
            if (num >= 50 && num < 90)
            {
                str += hash[50];
                num -= 50;
                continue;
            }
            if (num >= 40 && num < 50)
            {
                str += hash[40];
                num -= 40;
                continue;
            }

            if (num >= 10 && num < 40)
            {
                str += hash[10];
                num -= 10;
                continue;
            }
            if (num >= 9 && num < 10)
            {
                str +="IX";
                num -= 9;
                continue;
            }
            if (num == 4)
            {
                // cout<<"进来"<<endl;
                // cout<<"zhiqina"<<str<<endl;
                str +="IV";
                // cout<<"zhihou"<<str<<endl;
                num -= 4;
                continue;
            }
            if (num >= 5)
            {
                str += hash[5];
                num -= 5;
                continue;
            }

            if (num < 4)
            {
                str += hash[1];
                num -= 1;
                continue;
            }
        }
        return str;
    }
    string intToRoman(int num)
    {
        return num2Rom(num);
    }
};

int main(int argc, char **argv)
{
    Solution s;
    cout << s.intToRoman(4);
}