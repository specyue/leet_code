#include <string>
#include <map>
using namespace std;
class Solution
{
public:
    string multiply(string num1, string num2)
    {
        map<int, string> hash1;
        map<string,int> hash2;
        for (int i = 0; i <= __INT32_MAX__; i++)
        {
            hash1[i] = to_string(i);
        }
        for (int i = 0; i <= __INT32_MAX__; i++)
        {
            hash2[to_string(i)] = i;
        }
        return hash1[hash2[num1]+hash2[num2]];
    }
};