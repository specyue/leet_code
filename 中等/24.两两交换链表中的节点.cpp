#include <string>
#include <vector>
using namespace std;

struct ListNode
{
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(NULL) {}
};

//不是进行节点值的交换而是进行节点的交换
class Solution
{
public:
    ListNode *swapPairs(ListNode *head)
    {
        ListNode *h, *tail, *pre, *root;
        if (head == NULL)
        {
            return NULL;
        }
        // root = head;
        h = head;
        tail = head->next;
        if (tail == NULL)
        {
            //只有一个节点
            return head;
        }
        h->next = tail->next;
        tail->next = h;
        root = tail;
        pre = tail;

        pre = pre->next;
        h = h->next;
        if (h == NULL)
        {
            return root;
        }
        tail = tail->next->next->next;
        if (tail == NULL)
        {
            return root;
        }
        while (h != NULL && tail != NULL)
        {
            h->next = tail->next;
            tail->next = h;
            pre->next = tail;
            pre = pre->next->next;
            h = h->next;
            if (h == NULL)
            {
                return root;
            }
            tail = tail->next->next->next;
            if (tail == NULL)
            {
                return root;
            }
        }
        return root;
    }
};