#include <vector>
using  namespace std;
//暴力
class Solution {
    int max_hw = 0;
public:
    int maxArea(vector<int>& height) {
        int len = height.size();
        for(int i = 0;i<len;i++){
            for(int j = i;j<len;j++){
                int w = j-i;
                int h = min(height[i],height[j]);
                int hw = h*w;
                max_hw = max(max_hw,hw);
            }
        }
        return max_hw;
    }
};