#include <vector>
using namespace std;
class Solution
{
public:
    int searchInsert(vector<int> &nums, int target)
    {
        int left = 0;
        int right = nums.size() - 1;
        int mid;
        while (left <= right)
        {
            mid = (left + right) / 2;
            if (nums.at(mid) == target)
            {
                return mid;
            }
            if (nums.at(mid) > target)
            {
                right = mid-1;
            }
            if (nums.at(mid) < target)
            {
                left = mid+1;
            }
        }
        return left;
    }
};