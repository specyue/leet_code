#include<bits/stdc++.h>
using namespace std;

int main(int argc,char** argv){
    int n,k;
    cin>>n>>k;
 //   if(n = 0){
 //       return 0;
 //   }
    vector<int> a(n);
    for(int i = 0;i<n;i++){
        cin>>a[i];
    }
    
    int Max = 0;
    int l = 0;
    int r = n-1;
    int current = 0;
    int sum = 0;
    while(l <= r ){
        while(current<=r){
            sum+=a[current];
            if(sum == k){
                Max = max(Max,current - l+1);
            }
            current++;
        }
        l++;
        current = l;
        sum = 0;
    }
    cout<<Max<<endl;
    return 0;
}
