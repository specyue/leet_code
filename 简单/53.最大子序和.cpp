#include <vector>
using namespace std;
class Solution
{
public:
    int maxSubArray(vector<int> &nums)
    {
        vector<int> dp;
        int len = nums.size();
        dp.push_back(nums[0]);
        for(int i = 1;i<len;i++){
            if(dp[i-1]>0){
                dp.push_back(dp[i-1]+nums[i]);
            }else
            {
                dp.push_back(nums[i]);
            }
        }
        int dp_len = dp.size();
        int max_dp = dp[0];
        for(int i = 1;i<dp_len;i++){
            if(max_dp<dp[i]){
                max_dp = dp[i];
            }
        }
        return max_dp;
    }
};
//上面程序超时，想办法优化