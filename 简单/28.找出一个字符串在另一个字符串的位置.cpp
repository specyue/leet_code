#include <string>
using namespace std;
class Solution
{
public:
    int strStr(string haystack, string needle)
    {
        if (needle == "")
        {
            return 0;
        }
        int len = haystack.length();
        int n_len = needle.length();
        bool flag = true;
        int i = 0;
        while (i != len)
        {
            if (haystack[i] == needle[0])
            {
                for (int j = 0; j < n_len; j++)
                {
                    if (needle[j] != haystack[i + j])
                    {
                        // flag = false;
                        break;
                    }
                    if(j==n_len-1&&needle[j]==haystack[i+j]){
                        return i ;
                    }
                }
            }
            i++;
        }
        return -1;
    }
};