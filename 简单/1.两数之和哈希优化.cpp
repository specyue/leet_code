#include <vector>
#include <stdio.h>
#include <iostream>
#include <map>
using std::vector;
// using std::map
using namespace std;

class Solution
{
public:
    vector<int> twoSum(vector<int> &nums, int targrt)
    {
        vector<int> twoSum;
        map<int, int> temMap;
        for (int i = 0; i < nums.size(); i++)
        {
            temMap[nums[i]] = i;
        }
        for (int i = 0; i < nums.size(); i++)
        {
            if (temMap[targrt - nums[i]] != 0 && temMap[targrt - nums[i]] != i)
            {
                twoSum.push_back(i);
                twoSum.push_back(temMap[targrt - nums[i]]);
                break;
            }
        }
        return twoSum;
    }
};

int main(int argc, char **argv)
{
    Solution s;
    vector<int> nums = {1, 2, 3, 4, 5, 6, 7, 8, 9};
    vector<int> a;
    a = s.twoSum(nums, 9);
    for (int i = 0; i < a.size(); i++)
    {
        cout << a[i] << endl;
    }
}
