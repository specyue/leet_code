#include <string>
#include<iostream>
using namespace std;

class Solution
{
public:
    string countAndSay(int n)
    {
        n -= 1;
        string s = "1";
        string tmp = "";
        if (n == 0)
        {
            return s;
        }
        for (int i = 0; i < n; i++)
        {
            int count = 1;
            for (int j = 0; j < s.size(); j++)
            {

                if (j + 1 == s.size())
                {
                    tmp += to_string(count) + s[j];
                    count = 1;
                    break;
                }
                if (s[j] == s[j + 1])
                {
                    count += 1;
                }
                else
                {
                    tmp += to_string(count) + s[j];
                    count = 1;
                }
            }
            s = tmp;
            tmp = "";
        }
        return s;
    }
};

int main(int argc,char**argv){
    Solution s1;
    string s = s1.countAndSay(5);
    cout<<s<<endl;
}