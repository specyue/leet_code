// 要求空间复杂度为O(1)

#include <vector>
using namespace std;

class Solution
{
public:
    int removeDuplicates(vector<int> &nums)
    {
        int i = 1;
        for ( i ; i < nums.size(); i++)
        {
            if (nums.at(i) == nums.at(i - 1))
            {
                nums.erase(i+nums.begin());
                i = 1;
            }
        }
        for ( i ; i < nums.size(); i++)
        {
            if (nums.at(i) == nums.at(i - 1))
            {
                nums.erase(i+nums.begin());
                i = 1;
            }
        }
        return nums.size();
    }
};